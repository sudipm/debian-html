<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in denial of
service, sandbox bypass, information disclosure or the execution
of arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7u231-2.6.19-1~deb8u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1886.data"
# $Id: $
