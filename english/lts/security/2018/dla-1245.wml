<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in GraphicsMagick, a collection of
image processing tools, which may result in a denial of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5685">CVE-2018-5685</a>

    <p>An infinite loop and application hang has been discovered in the
    ReadBMPImage function (coders/bmp.c). Remote attackers could
    leverage this vulnerability to cause a denial of service via an
    image file with a crafted bit-field mask value.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.16-1.1+deb7u17.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1245.data"
# $Id: $
