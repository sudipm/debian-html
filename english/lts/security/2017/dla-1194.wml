<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16931">CVE-2017-16931</a>

      <p>parser.c in libxml2 before 2.9.5 mishandles parameter-entity
      references because the NEXTL macro calls the
      xmlParserHandlePEReference function in the case of a '%' character
      in a DTD name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16932">CVE-2017-16932</a>

      <p>parser.c in libxml2 before 2.9.5 does not prevent infinite
      recursion in parameter entities.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.0+dfsg1-7+wheezy11.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1194.data"
# $Id: $
