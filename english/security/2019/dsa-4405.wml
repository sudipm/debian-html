<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in openjpeg2, the
open-source JPEG 2000 codec, that could be leveraged to cause a denial
of service or possibly remote code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

    <p>Write stack buffer overflow in the jp3d and jpwl codecs can result
    in a denial of service or remote code execution via a crafted jp3d
    or jpwl file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

    <p>Integer overflow can result in a denial of service via a crafted bmp
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

    <p>Excessive iteration can result in a denial of service via a crafted
    bmp file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

    <p>Division-by-zero vulnerabilities can result in a denial of service via
    a crafted j2k file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

    <p>Null pointer dereference can result in a denial of service via a
    crafted bmp file.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 2.1.2-1.1+deb9u3.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>For the detailed security status of openjpeg2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"
# $Id: $
