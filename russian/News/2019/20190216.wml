#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 9: выпуск 9.8</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о восьмом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих
пакетов:</p>
<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction arc "Исправление обхода каталогов [CVE-2015-9275], аварийной остановки arcdie при вызове с более, чем 1 переменным аргументом и чтением arc-заголовка версии 1">
<correction astroml-addons "Исправление Python3-зависимостей">
<correction base-files "Обновление для текущей редакции">
<correction c3p0 "Исправление внешней сущности XML [CVE-2018-20433]">
<correction ca-certificates-java "Исправление создания временных файлов jvm-*.cfg на armhf">
<correction chkrootkit "Исправление регулярного выражения для фильтрации ошибочного срабатывания для dhcpd и dhclient в тесте анализатора пакетов">
<correction compactheader "Обновление для работы с более новыми версиями Thunderbird">
<correction courier "Исправление подстановки @piddir@">
<correction cups "Исправления безопасности [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config "Исправление настройки персональных веб-страниц; повторное включение автономной установки сервера combi, включая поддержку бездисковых рабочих станций; включение настройки домашней страницы Chromium во время установки и через LDAP">
<correction debian-installer "Повторная сборка для текущей редакции">
<correction debian-installer-netboot-images "Повторная сборка с целью поддержки раздела proposed-updates">
<correction debian-security-support "Обновление статуса поддержки различных пакетов">
<correction dnspython "Исправление ошибки при выполнении грамматического разбора побитового отображения nsec3 из текста">
<correction egg "Пропуск сценария emacsen-install для неподдерживаемого пакета xemacs21">
<correction erlang "Не устанавливать режим Erlang для XEmacs">
<correction espeakup "debian/espeakup.service: исправление совместимости с предыдущими версиями systemd">
<correction freerdp "Исправление проблем безопасности [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788]; добавление поддержки CredSSP v3 и RDP proto v6">
<correction ganeti-os-noop "Исправление определения размера для отличных от блочных устройств">
<correction glibc "Исправление нескольких проблем безопасности [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237]; предотвращение ошибки сегментирования на ЦП с AVX512-F; исправление использования указателей после освобождения памяти в функции pthread_create(); проверка postgresql в проверке NSS; исправление функции pthread_cond_wait() в случае pshared на отличных от x86 архитектурах">
<correction gnulib "vasnprintf: исправление выхода за пределы выделенного буфера памяти [CVE-2018-17942]">
<correction gnupg2 "Предотвращение аварийной остановки при импорте ключей без TTY">
<correction graphite-api "Исправление правописания RequiresMountsFor в службе systemd">
<correction grokmirror "Добавление отсутствующей зависимости от python-pkg-resources">
<correction gvrng "Исправление проблемы прав доступа, мешающей запускать gvrng; порождение правильных Python-зависимостей">
<correction ibus "Исправление мультиархитектурных установок путём удаления Python-зависимости пакета gir">
<correction icinga2 "Исправление временных меток, сохраняемых в виде локального времени в PostgreSQL">
<correction intel-microcode "Добавление накопленных исправлений для Westmere EP (подпись 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort "Исправление Python-зависимостей">
<correction jdupes "Исправление потенциальных аварийных остановок на ARM">
<correction kmodpy "Удаление неправильного поля Multi-Arch: same из python-kmodpy">
<correction libapache2-mod-perl2 "Запрет разделов &lt;Perl&gt; в контролируемых пользователями настройках [CVE-2011-2767]">
<correction libb2 "Обнаружение того, что система может ли использовать AVX до момента самого использования">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libemail-address-list-perl "Исправление отказа в обслуживании [CVE-2018-18898]">
<correction libemail-address-perl "Исправление отказа в обслуживании [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod "python-gpod: добавление отсутствующей зависимости от python-gobject-2">
<correction libssh "Исправление сломанной аутентификации с клавиатурой на стороне сервера">
<correction linux "Новый выпуск основной ветки разработки; новая версия основной ветки; исправление ошибок сборки на arm64 и mips*; libceph: исправление проверки CEPH_FEATURE_CEPHX_V2 в функции calc_signature()">
<correction linux-igd "Зависимость сценария инициализации от $network">
<correction lttng-modules "Исправление сборки на ядрах linux-rt 4.9 и ядрах с версией &gt;= 4.9.0-3">
<correction mistral "Исправление ошибки <q>действие std.ssh может раскрыть наличие произвольных файлов</q> [CVE-2018-16849]">
<correction monkeysign "Исправление ошибки безопасности [CVE-2018-12020]; фактическая отправка нескольких сообщений электронной почты вместо одного сообщения">
<correction mpqc "Установка также и sc-libtool">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки">
<correction nvidia-modprobe "Новый выпуск основной ветки разработки">
<correction nvidia-persistenced "Новый выпуск основной ветки разработки">
<correction nvidia-settings "Новый выпуск основной ветки разработки">
<correction nvidia-xconfig "Новый выпуск основной ветки разработки">
<correction openni2 "Исправление нарушения базовой строки на armhf и ошибки сборки на armel из-за использования NEON">
<correction openvpn "Исправление поведения NCP при повторном соединении TLS, вызывающего ошибки <q>AEAD Decrypt error: cipher final failed</q>">
<correction parsedatetime "Добавление поддержки для Python 3">
<correction pdns "Исправление проблем безопасности [CVE-2018-1046 CVE-2018-10851]; исправление MySQL-запросов с сохраняемыми процедурами; исправление обнаружения доменов при использовании движков LDAP, Lua, OpenDBX">
<correction pdns-recursor "Исправление проблем безопасности [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage "Добавление отсутствующей зависимости от gir1.2-gtk-3.0">
<correction postfix "Новый стабильный выпуск основной ветки разработки; предотвращение ошибок postconf в случае, если postfix-instance-generator запускается во время загрузки системы">
<correction postgresql-9.6 "Новый выпуск основной ветки разработки">
<correction postgrey "Повторная сборка без изменений">
<correction pylint-django "Исправление Python3-зависимостей">
<correction python-acme "Обратный перенос более новой версии в связи с устареванием tls-sni-01">
<correction python-arpy "Исправление Python3-зависимостей">
<correction python-certbot "Обратный перенос более новой версии в связи с устареванием tls-sni-01">
<correction python-certbot-apache "Обратный перенос более новой версии в связи с устареванием tls-sni-01">
<correction python-certbot-nginx "Обратный перенос более новой версии в связи с устареванием tls-sni-01">
<correction python-hypothesis "Исправление (расположенных в обратном порядке) зависимостей в пакетах python3-hypothesis и python-hypothesis-doc">
<correction python-josepy "Новый пакет, требуется для Certbot">
<correction pyzo "Добавление отсутствующей зависимости от python3-pkg-resources">
<correction r-cran-readxl "Исправление аварийных остановок [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit "Перенос dbus и polkit из поля Recommends в поле Depends">
<correction ruby-rack "Исправление возможного межсайтового скриптинга [CVE-2018-16471]">
<correction samba "Новый выпуск основной ветки разработки; s3:ntlm_auth: исправление утечки памяти в функции manage_gensec_request(); игнорирование ошибок запуска nmbd в случае отсутствия интерфейсов, отличных от интерфейса обратной петли, или отсутствия локальных IPv4-интерфейсов, отличных от интерфейса обратной петли; исправление регрессии CVE-2018-14629 для отличных от CNAME записей">
<correction sl-modem "Поддержка Linux версий &gt; 3">
<correction sogo-connector "Обновление для работы с новыми версиями Thunderbird">
<correction sox "Фактическое применение исправлений для CVE-2014-8145">
<correction ssh-agent-filter "Исправление двухбайтовой записи за пределы выделенного стека">
<correction supercollider "Отключение поддержки XEmacs и Emacs &lt;=23">
<correction sympa "Удаление /etc/sympa/sympa.conf-smime.in из файлов настройки; исправление полного пути для команды head в файле настройки Sympa">
<correction twitter-bootstrap3 "Исправление многочисленных уязвимостей [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction uglifyjs "Исправление содержимого справочной страницы">
<correction uriparser "Исправление многочисленных уязвимостей [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm "Удаление поддержки xemacs21">
<correction vulture "Добавление отсутствующей зависимости от python3-pkg-resources">
<correction wayland "Исправление возможного переполнения целых чисел [CVE-2017-16612]">
<correction wicd "Постоянная зависимость от net-tools, а не через альтернативы">
<correction wvstreams "Обходное решение проблемы с повреждением стека">
<correction xapian-core "Исправление утечек блоков freelist в патологических случаях, о которой Database::check() сообщает как об ошибке <q>DatabaseCorruptError</q>">
<correction xkeycaps "Предотвращение ошибок сегментирования в commands.c в случаях, когда имеется более 8 KEYSYM-позиций на одну клавишу">
<correction yosys "Исправление ошибки <q>ModuleNotFoundError: No module named 'smtio'</q>">
<correction z3 "Удаление неправильного поля Multi-Arch: same из пакета python-z3">
</table>

<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction adblock-plus "Несовместим с новыми версиями firefox-esr">
<correction calendar-exchange-provider "Несовместим с новыми версиями Thunderbird">
<correction cookie-monster "Несовместим с новыми версиями firefox-esr">
<correction corebird "Сломан из-за изменения API Twitter">
<correction debian-buttons "Несовместим с новыми версиями firefox-esr">
<correction debian-parl "Зависит от сломанного/удалённого дополнения Firefox">
<correction firefox-branding-iceweasel "Несовместим с новыми версиями firefox-esr">
<correction firefox-kwallet5 "Несовместим с новыми версиями firefox-esr">
<correction flashblock "Несовместим с новыми версиями firefox-esr">
<correction flickrbackup "Несовместим с текущим API Flickr">
<correction imap-acl-extension "Несовместим с новыми версиями firefox-esr">
<correction libwww-topica-perl "Бесполезен из-за закрытия сайта Topica">
<correction mozilla-dom-inspector "Несовместим с новыми версиями firefox-esr">
<correction mozilla-noscript "Несовместим с новыми версиями firefox-esr">
<correction mozilla-password-editor "Несовместим с новыми версиями firefox-esr">
<correction mozvoikko "Несовместим с новыми версиями firefox-esr">
<correction personaplus "Несовместим с новыми версиями firefox-esr">
<correction python-formalchemy "Непригоден к использованию, не может быть импортирован в Python">
<correction refcontrol "Несовместим с новыми версиями firefox-esr">
<correction requestpolicy "Несовместим с новыми версиями firefox-esr">
<correction spice-xpi "Несовместим с новыми версиями firefox-esr">
<correction toggle-proxy "Несовместим с новыми версиями firefox-esr">
<correction y-u-no-validate "Несовместим с новыми версиями firefox-esr">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>


<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
