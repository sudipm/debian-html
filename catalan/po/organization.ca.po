# organization webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2011, 2017-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2019-05-12 19:15+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correu de delegació"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correu de nomenament"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membre"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "gestor"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Administrador de llançament de la versió estable"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "mag"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
msgid "chair"
msgstr "president"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "secretari"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Directors"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Distribució"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr "Comunicació i divulgació"

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr "Equip de protecció de dades"

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
msgid "Publicity team"
msgstr "Equip de publicitat"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Suport i infraestructura"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Comitè tècnic"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Secretari"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Projectes de desenvolupament"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "Arxius FTP"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr "Mestres de FTP"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "Assistents de FTP"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr "Mags de FTP"

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Gestió de llançaments"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr "Equip de llançament"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Control de qualitat"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Equip del sistema d'instal·lació"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr "Equip del sistema autònom de Debian"

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Notes de la versió"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "Imatges de CD"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Producció"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Proves"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr "Equip en núvol"

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr "Infraestructura d'autocompilació"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr "Equip de wanna-build"

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr "Administració dels dimonis de compilació"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Documentació"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "Llista de paquets en perspectiva o en falta de feina"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Arquitectures"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Configuracions especials"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Portàtils"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Tallafocs"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Sistemes empotats"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Contacte de premsa"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Pàgines web"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr "Divulgació"

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr "Projecte Debian dona"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr "Anti-assetjament"

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Esdeveniments"

#: ../../english/intro/organization.data:289
msgid "DebConf Committee"
msgstr "Comitè de DebConf"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Programa de socis"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Coordinació de donacions de maquinari"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Suport d'usuari"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Sistema de Seguiment d'Errors"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administració i arxius de les llistes de correu"

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr "Recepció de nous membres"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Administradors dels comptes de Debian"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Per enviar un missatge privat al grup de DAMs, empreu la clau GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantenidors de l'anell de claus (PGP i GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Equip de seguretat"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Pàgina de consultors"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "Pàgina de proveïdors de CD"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Política"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Administració del sistema"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Aquesta és l'adreça a utilitzar quan es troben problemes en una màquina de "
"Debian, incloent problemes amb contrasenyes o si necessiteu la instal·lació "
"d'un paquet."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Si teniu problemes de hardware en màquines de Debian, mireu la pàgina de <a "
"href=\"https://db.debian.org/machines.cgi\">Màquines de Debian</a>, hauria "
"de contenir informació de l'administrador de cada màquina."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador del directori LDAP de desenvolupadors"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Rèpliques"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "Mantenidor del DNS"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Sistema de seguiment de paquets"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr "Tresorers"

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Peticions d'us de la <a name=\"trademark\" href=\"m4_HOME/trademark\">marca "
"comercial</a>"

#: ../../english/intro/organization.data:491
msgid "Salsa administrators"
msgstr "Administració de Salsa"

#~ msgid "Debian Pure Blends"
#~ msgstr "Barreges pures de Debian"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian per a nens de 1 a 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian per a la pràctica i recerca medica"

#~ msgid "Debian for education"
#~ msgstr "Debian per a l'educació"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian a les oficines de dret"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian per a gent amb discapacitats"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian per a la ciència i recerca relacionada"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian per a l'astronomia"

#~ msgid "Live System Team"
#~ msgstr "Equip del sistema autònom"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Publicity"
#~ msgstr "Publicitat"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Equip de l'anell de claus dels mantenidors de Debian"

#~ msgid "Volatile Team"
#~ msgstr "Equip de llançament da la versió volàtil"

#~ msgid "Vendors"
#~ msgstr "Venedors"

#~ msgid "APT Team"
#~ msgstr "Equip APT"

#~ msgid "Release Assistants"
#~ msgstr "Assistents de llançament"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Assistents de llançaments per a «stable»"

#~ msgid "Release Wizard"
#~ msgstr "Mag de llançaments"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribució multimedia de Debian"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Aquest encara no és un projecte intern oficial de Debian però ha anunciat "
#~ "la intenció de ser-ho."

#~ msgid "Mailing List Archives"
#~ msgstr "Arxius de les llistes de correu"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema d'instal·lació per a «stable»"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux per a la computació d'empreses"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian per organitzacions sense anim de lucre"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "El sistema operatiu universal com el seu escriptori"

#~ msgid "Accountant"
#~ msgstr "Contable"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinació de signatura de claus"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Els noms individuals dels administradors dels dimonis de compilació es "
#~ "poden trobar a <a href=\"http://www.buildd.net\">http://www.buildd.net</"
#~ "a>. Seleccioneu una arquitecture i una distribució per veure els dimonis "
#~ "de compilació disponibles i els seus administradors."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Els administradors responsables dels dimonis de compilació per a una "
#~ "arquitectura particular, es poden contactar a <genericemail arch@buildd."
#~ "debian.org>, per exemple <genericemail i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Equip de màrqueting"

#~ msgid "Handhelds"
#~ msgstr "Ordinadors de ma"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equip de llançaments per a «stable»"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribució de Debian adaptada"

#~ msgid "Security Audit Project"
#~ msgstr "Projecte d'auditoria de seguretat"

#~ msgid "Testing Security Team"
#~ msgstr "Equip de seguretat de Proves"

#~ msgid "Alioth administrators"
#~ msgstr "Administració d'Alioth"

#~ msgid "Individual Packages"
#~ msgstr "Paquets individuals"
