# translation of stats.po to Swedish
msgid ""
msgstr ""
"Project-Id-Version: stats\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Felaktig översättningsversion"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Den här översättningen är föråldrad"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Originalet är nyare än denna översättning"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Originalet finns inte längre"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "träffar"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "träffräknare N/A"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Klicka för att hämta diffstat-data"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Skapad med <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Översättningssammanfattning för"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Översatt"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Uppdaterade"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Föråldrade"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Ej översatt"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "filer"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Observera: listorna på den här sidan sorteras efter popularitet. Hovra över "
"sidnamnet för att se antal träffar."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Föråldrade översättningar"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Fil"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git-kommandorad:"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Logg"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Översättning"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Underhållare"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Översättare"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Generella sidor ej översatta"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Ej översatta generella sidor"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nyheter som inte översatts"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Oöversatta nyheter"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Konsult/användarsidor som inte översatts"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Ej översatta Konsult/användarsidor"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Internationella sidor som ej översatts"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Ej översatta internationella sidor"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Översatta sidor (uppdaterade)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Översatta mallar (PO-filer)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Statistik för PO-översättningar"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Luddig"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Ej översatt"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Totalt"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Totalt:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Översatta webbsidor"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Översättningsstatistik efter antal sidor"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Språk"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Översättningar"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Översatta webbsidor (efter storlek)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Översättningsstatistik efter sidstorlek"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistik över översättningar på Debians webbplats"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Det finns %d sidor att översätta."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Det finns %d bytes att översätta."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Det finns %d strängar att översätta."

#~ msgid "Unified diff"
#~ msgstr "Samlad diff"

#~ msgid "Colored diff"
#~ msgstr "Färglagd diff"

#~ msgid "Commit diff"
#~ msgstr "Commit diff"

#~ msgid "Created with"
#~ msgstr "Skapad med"
