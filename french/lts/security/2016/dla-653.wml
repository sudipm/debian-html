#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7161">CVE-2016-7161</a>

<p>Un dépassement de tas dans le rappel de .receive de
xlnx.xps-ethernetlite dans qemu-kvm permet à des attaquants d'exécuter du
code arbitraire sur l'hôte de QEMU à l'aide d'un paquet ethlite trop grand.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7170">CVE-2016-7170</a>

<p>La fonction vmsvga_fifo_run dans hw/display/vmware_vga.c dans qemu-kvm
est vulnérable à un accès mémoire hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7908">CVE-2016-7908</a>

<p>La fonction mcf_fec_do_tx dans hw/net/mcf_fec.c dans qemu-kvm ne limite
pas correctement le compte de descripteurs de tampon lors de la transmission
de paquets. Cela permet à des administrateurs de l’OS local client de
provoquer un déni de service (boucle infinie et plantage du processus QEMU)
au moyen de vecteurs impliquant un descripteur de tampon d'une longueur
de 0 et de valeurs contrefaites dans bd.flags.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-653.data"
# $Id: $
