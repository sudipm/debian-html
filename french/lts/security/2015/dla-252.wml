#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs bogues ont été découverts dans PostgreSQL, un système de
serveur de bases de données relationnelles. La branche 8.4 a atteint sa fin
de vie pour l’amont, mais elle est encore présente dans Squeeze de Debian.
Cette nouvelle version mineure de LTS fournit les correctifs appliqués par
l’amont à la version 9.0.22, rétroportée vers la version 8.4.22 qui était
la dernière version publiée officiellement par les développeurs de
PostgreSQL. Cette initiative de LTS pour Squeeze-lts est un projet de la
communauté parrainé par credativ GmbH.</p>

<p>## Migration vers la version 8.4.22lts4</p>

<p>Un vidage et restauration n’est pas requis pour ceux qui utilisent une
version 8.4.X. Néanmoins si vous mettez à niveau à partir d'une version
antérieure à 8.4.22, consultez les notes de publication correspondantes.</p>

<p>## Modifications</p>

<ul>

<li>Correction d'un échec rare d'invalidation du fichier init du cache de relation (Tom Lane)

<p>Avec seulement une mauvaise temporalité d'activités concurrentes, une
commande VACUUM FULL sur un catalogue du système pourrait échouer à mettre à
jour le fichier init qui est utilisé pour éviter la tâche de charger le
cache pour de nouvelles sessions. Cela pourrait avoir pour conséquence que
les sessions ultérieures pourraient n'avoir accès à aucun catalogue. C'est
un bogue très ancien, mais il est si difficile à déclencher qu'aucun
cas reproductible n'a été découvert jusqu'à récemment.</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 8.4.22lts4-0+deb6u1 de postgresql-8.4.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2015/dla-252.data"
# $Id: $
