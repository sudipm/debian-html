#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6307">CVE-2017-6307</a>

<p>Un problème a été découvert dans tnef avant 1.4.13. Deux écritures hors
limites ont été relevées dans src/mapi_attr.c:mapi_attr_read(). Cela pourrait
conduire à des opérations de lecture et écriture non valables, contrôlées par
un attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6308">CVE-2017-6308</a>

<p>Un problème a été découvert dans tnef avant 1.4.13. Plusieurs dépassements
d'entier, qui peuvent conduire à des dépassements de tas, ont été relevés dans
les fonctions que enveloppent l’allocation mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6309">CVE-2017-6309</a>

<p>Un problème a été découvert dans tnef avant 1.4.13. Deux confusions de type
ont été relevées dans la fonction parse_file(). Cela pourrait conduire à des
opérations de lecture et écriture non valables, contrôlées par un attaquant.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6310">CVE-2017-6310</a>

<p>Un problème a été découvert dans tnef avant 1.4.13. Quatre confusions de type
ont été relevées dans la fonction file_add_mapi_attrs(). Cela pourrait conduire
à des opérations de lecture et écriture non valables, contrôlées par un
attaquant.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.4.9-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tnef.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-839.data"
# $Id: $
