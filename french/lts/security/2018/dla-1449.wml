#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes ont été découverts dans OpenSSL, la boîte à outils associée à
SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0732">CVE-2018-0732</a>

<p>Déni de service par un serveur malveillant envoyant une première valeur très
large au client lors de l’initialisation de connexion TLS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0737">CVE-2018-0737</a>

<p>Alejandro Cabrera Aldaya, Billy Brumley, Cesar Pereida Garcia et Luis Manuel
Alvarez Tapia ont découvert que l’algorithme de génération de clef RSA d’OpenSSL
a été démontré être vulnérable à une attaque temporelle sur le cache. Un
attaquant avec accès suffisant pour monter de telles attaques durant le
processus de génération pourrait récupérer la clef privée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.0.1t-1+deb8u9.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1449.data"
# $Id: $
