#use wml::debian::translation-check translation="da347ceee9cca800740ef75deed5e600ef8e2b1d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans openjpeg2, le codec
JPEG 2000 libre, qui pourraient être exploitées pour provoquer un déni de
service ou éventuellement l'exécution de code distant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17480">CVE-2017-17480</a>

<p>Un dépassement de tampon de pile en écriture dans les codecs jp3d et
jpwl peut avoir pour conséquence un déni de service ou l'exécution de code
distant à l'aide d'un fichier jp3d ou jpwl contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5785">CVE-2018-5785</a>

<p>Un dépassement d'entier peut avoir pour conséquence un déni de service à
l'aide d'un fichier bmp contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

<p>Une itération excessive peut avoir pour conséquence un déni de service à
l'aide d'un fichier bmp contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

<p>Des vulnérabilités de division par zéro peuvent avoir pour conséquence
un déni de service à l'aide d'un fichier j2k contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18088">CVE-2018-18088</a>

<p>Un déréférencement de pointeur NULL peut avoir pour conséquence un déni
de service à l'aide d'un fichier bmp contrefait.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 2.1.2-1.1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjpeg2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjpeg2">\
https://security-tracker.debian.org/tracker/openjpeg2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4405.data"
# $Id: $
