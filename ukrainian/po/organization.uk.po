# translation of organization.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: organization\n"
"PO-Revision-Date: 2017-11-18 16:30+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: українська <Ukrainian <ukrainian>>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>делегат"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>делегат"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "чинний"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "член"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "керівник"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Керівник стабільного випуску"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "КСВ"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "чарівник"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "голова"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "помічник"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "секретар"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Керівники"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr "Зв'язок та інформаційна підтримка"

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
msgid "Publicity team"
msgstr "Команда зв'язків з громадськістю"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Підтримка та інфраструктура"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Лідер"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Технічний комітет"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Секретар"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Проекти розробки"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "FTP-архіви"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr "FTP-майстри"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "FTP-помічники"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr "FTP-чарівники"

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr "Зворотнє перенесення"

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr "Команда зворотнього перенесення"

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Керування випуском"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr "Команда, відповідальна за випуск"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Забезпечення якості"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Команда системи встановлення"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Інформація про випуск"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "Образи компакт-дисків"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Виробництво"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Тестування"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr "Інфраструктура автоматичного збирання"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr "Команда підготовки до збирання"

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr "Адміністрування збирання"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Документація"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "Список пакунків що потребують доробки або плануються"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Перенесення"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Спеціальні конфігурації"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Лептопи"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Брандмауери"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Вбудовані системи"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Контакт для преси"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Веб-сторінки"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr "Планета Debian"

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr "Інформаційна підтримка"

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr "Проект Жінки Debian"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr "Анти-домагання"

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Події"

#: ../../english/intro/organization.data:289
msgid "DebConf Committee"
msgstr "Комітет DebConf"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Партнерська програма"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Координація пожертвування обладнання"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Підтримка користувачів"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Система відслідковування помилок"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Керування списками розсилки та архіви списків розсилки"

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr "Реєстраційний стіл нових членів"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Керівники облікових записів Debian (КОЗД)"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Щоб надіслати приватне повідомлення усім КОЗД, використовуйте ключ GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Супроводжуючі зв'язок ключів (PGP та GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Команда безпеки"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Сторінка консультантів"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "Сторінка постачальників компакт-дисків"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Політика"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Системне адміністрування"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Це адреса, яку потрібно використовувати у випадку виникнення проблем з "
"машинами Debian, включно з проблемами з паролями, або якщо вам потрібно "
"встановити пакунок."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Якщо ви помітили проблему з апаратурою машини Debian, перегляньте сторінку "
"<a href=\"https://db.debian.org/machines.cgi\">Машини Debian</a>, вона "
"повинна містити інформацію про адміністраторів кожної машини."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "Адміністратор LDAP-каталогу розробників"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Дзеркала"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "Супроводжуючий DNS"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Система відслідковування пакунків"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Запити на використання <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">торгової марки</a>"

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Адміністратори Alioth"

#~ msgid "Debian Pure Blends"
#~ msgstr "Чисті суміші Debian"

#~ msgid "Individual Packages"
#~ msgstr "Окремі пакунки пакунки"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian для дітей від 1 до 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian для медичної практики та досліджень"

#~ msgid "Debian for education"
#~ msgstr "Debian для освіти"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian в адвокатурах"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian для людей з обмеженими можливостями"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian для науки та суміжних досліджень"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian для освіти"

#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Команда робочої системи"

#~ msgid "Auditor"
#~ msgstr "Ревізор"

#~ msgid "Publicity"
#~ msgstr "Зв'язки з громадскістю"

#~ msgid "Release Assistants"
#~ msgstr "Помічники керівника випуском"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Керівник випуском „stable“"

#~ msgid "Vendors"
#~ msgstr "Постачальники"

#~ msgid "APT Team"
#~ msgstr "Команда APT"

#~ msgid "Mailing List Archives"
#~ msgstr "Архіви списків розсилки"

#~ msgid "Security Testing Team"
#~ msgstr "Команда безпеки тестового дистрибутиву"

#~ msgid "Key Signing Coordination"
#~ msgstr "Координація підписування ключів"

#~ msgid "Accountant"
#~ msgstr "Рахівник"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Універсальна операційна система як робочій стіл"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian для некомерційних організацій"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian для підприємств"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Мультимедійний дистрибутив Debian"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Це ще не є офіційний внутрішній проект Debian, але оголошення про "
#~ "прагнення інтеграції."

#~ msgid "Security Audit Project"
#~ msgstr "Проект перевірки безпеки"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Команда безпеки"

#~ msgid "Alioth administrators"
#~ msgstr "Адміністратори Alioth"
