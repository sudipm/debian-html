#use wml::debian::template title="Δωρεές"
#use wml::debian::translation-check translation="ee65738645784ee4e456adb5d6051e23caaebb65" maintainer="galaxico"

<p>
Οι δωρεές, τις οποίες διαχειρίζεται ο 
<a href="$(HOME)/devel/leader">Επικεφαλής του Σχεδίου Debian</a> (DPL)
επιτρέπουν στο Debian να έχει
<a href="https://db.debian.org/machines.cgi">μηχανήματα</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">και άλλο υλικό</a>,
τομείς στο Διαδίκτυο, πιστοποιητικά κρυπτογραφίας,
<a href="http://www.debconf.org">το Συνέδριο του Debian</a>,
<a href="https://wiki.debian.org/MiniDebConf">μικρά συνέδρια του Debian</a>,
<a href="https://wiki.debian.org/Sprints">"σπριντ" προγραμματισμού</a>,
παρουσία σε άλλες διοργανώσεις και γεγονότα και άλλα πράγματα. Ευχαριστούμε 
όλους τους <a href="#donors">δωρητές</a> μας για την υποστήριξή τους στο 
Debian!
</p>

<p id="default">
Η ευκολότερη μέθοδος δωρεάς προς το Debian είναι μέσω του PayPal
υπέρ του <a href="https://www.spi-inc.org/" title="SPI">Software in the Public 
Interest</a>, ενός μη κερδοσκοπικού οργανισμού που διατηρεί περιουσιακά 
στοιχεία σε καταπίστευμα για λογαριασμό του Debian.
Μπορείτε επίσης να κάνετε δωρεά μέσω των <a 
href="#methods">μεθόδων που αναφέρονται παρακάτω</a>.
<!--
You can donate via the <a href="#methods">methods listed below</a>.
-->
</p>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<div>
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="86AHSZNRR29UU">
<input type="submit" value="Donate via PayPal" />
</div>
</form>

<!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
<form method="post" action="https://www.usaepay.com/interface/epayform/">
<div>
  <span>$<input type="text" name="UMamount" size="6"> USD, paid once only</span>
  <input name="UMdescription" value="Debian general contribution" type="hidden">
  <input name="UMkey" value="ijWB0O98meGpX0LrCP0eb1Y94sI1Dl67" type="hidden">
  <input name="UMcommand" value="sale" type="hidden" />
  <input type="submit" tabindex="2" value="Donate" />
</div>
</form>
-->

<h2 id="methods">Μέθοδοι δωρεών</h2>

<p>
Διάφοροι 
<a 
href="https://wiki.debian.org/Teams/Treasurer/Organizations">οργανισμοί</a>
διατηρούν περιουσιακά στοιχεία σε καταπίστευμα για λογαριασμό του Debian και 
δέχονται δωρεές εξ ονόματός του.
</p>

<table>
<tr>
<th>Οργανισμός</th>
<th>Μέθοδοι</th>
<th>Σημειώσεις</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
<!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
 <a href="#spi-usa-epay">USA ePay</a>,
-->
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a> (τακτικές δωρεές),
 <a href="#spi-cheque">επιταγή</a> (σε δολάρια ΗΠΑ/Καναδά),
 <a href="#spi-other">άλλοι τρόποι</a>
</td>
<td>ΗΠΑ, απαλλαγή φορολόγησης μη-κερδοσκοπικός</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">έμβασμα</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>Γαλλία, απαλλαγή φορολόγησης μη-κερδοσκοπικός</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">έμβασμα</a>,
 <a href="#debianch-other">άλλοι τρόποι</a>
</td>
<td>Ελβετία, μη-κερδοσκοπικός</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">εξοπλισμός</a>,
 <a href="#debian-time">χρόνος</a>,
 <a href="#debian-other">άλλοι τρόποι</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h3 id="spi">Software in the Public Interest</h3>

<p>
Το <a href="https://www.spi-inc.org/" title="SPI">Software in the Public 
Interest, Inc.</a> είναι ένα μη κερδοσκοπικό νομικό πρόσωπο που εξαιρείται από 
τη φορολογία με βάση στις Ηνωμένες Πολιτείες της Αμερικής, που ιδρύθηκε από 
ανθρώπους του Debian το 1997 για να βοηθήσει τους οργανισμούς του ελεύθερου 
λογισμικού/υλικού.
</p>

<h4 id="spi-paypal">PayPal</h4>
<p>
Εφάπαξ και τακτικές δωρεές μπορούν να γίνουν μέσω της σελίδας
<a 
href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">του SPI</a>
στον ιστότοπο του PayPal.
Για να κάνετε μια τακτική (επαναλαμβανόμενη) δωρεά, επιλέξτε το κουτί "Make 
This Recurring (Monthly)".
</p>

<h4 id="spi-click-n-pledge">Click &amp; Pledge</h4>

<p>
Εφάπαξ και τακτικές δωρεές μπορούν να γίνουν μέσω της σελίδας
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">του 
SPI</a>
στον ιστότοπο του the Click &amp; Pledge.
Για να κάνετε μια τακτική δωρεά, επιλέξτε πόσο συχνά θέλετε να κάνετε μια δωρεά 
στο δεξί μέρος, και κατεβάστε τον κέρσορα μέχρι το πεδίο "Debian Project 
Donation", εισάγετε το ποσό που θα θέλατε να κάνετε τη δωρεά, πατήστε το 
στοιχείο "Add to cart" και συνεχίστε με το υπόλοιπο της διαδικασίας.
</p>

<!--
Unfortunately this isn't possible to do yet:
<form method="post" action="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">
<input name="ScriptManager1" value="UpdatePanel1|btnDAddToCart_dnt_33979" type="hidden">
$<input name="DAmount_33979" size="6" id="DAmount_33979" type="text"> USD, paid
<select name="cboxRecurring" id="cboxRecurring">
  <option value="-1">once only</option>
  <option value="0">weekly</option>
  <option value="1">fortnightly</option>
  <option value="2">monthly</option>
  <option value="3">every 2 months</option>
  <option value="4">quarterly</option>
  <option value="5">bi-annually</option>
  <option value="6">annually</option>
</select>
<input type="submit" name="btnDAddToCart_dnt_33977" id="btnDAddToCart_dnt_33977" value="Donate"/>
</form>
-->

<h4 id="spi-cheque">Επιταγές</h4>

<p>
Δωρεές μπορούν να γίνουν μέσω επιταγής ή χρηματικών εντολών σε
<abbr title="US dollars">δολάρια ΗΠΑ, USD</abbr> και
<abbr title="Canadian dollars">δολάρια Καναδά, CAD</abbr>.
Παρακαλούμε συμπληρώστε το Debian στο πεδίο "memo" και στείλτε το στο SPI στη 
διεύθυνση που αναφέρονται στη σελίδα 
<a href="https://www.spi-inc.org/donations/">δωρεές στο SPI</a>.
</p>

<h4 id="spi-other">Άλλοι τρόποι</h4>

<p>
Δωρεές μέσω εμβάσματος και άλλες μεθόδους είναι επίσης διαθέσιμες.
Για μερικά μέρη του κόσμου ίσως είναι ευκολότερο να κάνετε μια δωρεά σε έναν 
από τους συνεργαζόμενους οργανισμούς του Software in the Public Interest.
Για περισσότερες λεπτομέρειες παρακαλούμε επισκεφθείτε τη σελίδα
<a href="https://www.spi-inc.org/donations/">δωρεές στο SPI</a>.
</p>

<h3 id="debianfrance">Debian France</h3>

<p>
Το <a href="https://france.debian.net/">Debian France Association</a> είναι 
ένας οργανισμός καταχωρημένος στη Γαλλία υπό τον <q>νόμο του 1901</q>,
πιυ ιδρύθηκε με σκοπό την υποστήριξη και την προαγωγή του Σχεδίου Debian 
στη Γαλλία.
</p>

<h4 id="debianfrance-bank">Έμβασμα</h4>
<p>
Δωρεές μέσω εμβασμάτων είναι διαθέσιμες στον τραπεζικό λογαριασμό που 
αναφέρεται στη σελίδα 
<a href="https://france.debian.net/soutenir/#compte">δωρεές στο Debian France
</a>. Αποδείξεις για τις δωρεές αυτές είναι διαθέσιμες με αποστολή μηνύματος 
στην ηλεκτρονική διεύθυνση
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h4 id="debianfrance-paypal">PayPal</h4>
<p>
Δωρεές μπορούν να σταλούν μέσω της σελίδας
<a 
href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal</a>.
Αυτές μπορούν να κατευθυνθούν προς το Debian France ειδικά ή το Σχέδιο Debian
γενικά.
</p>

<h3 id="debianch">debian.ch</h3>

<p> Το
<a href="https://debian.ch/">debian.ch</a> ιδρύθηκε για να αντιπροσωπεύει το 
Σχέδιο Debian στην Ελβετία και το Πριγκιπάτο του Λιχτνεστάιν.
</p>

<h4 id="debianch-bank">Έμβασμα</h4>

<p>
Δωρεές μέσω εμβασμάτων τόσο από ελβετικές όσο και διεθνείς τράπεζες 
είναι διαθέσιμες στους τραπεζικούς λογαριασμούς που αναφέρονται στον ιστότοπο 
του
<a href="https://debian.ch/">debian.ch</a>.
</p>

<h4 id="debianch-other">Άλλοι τρόποι</h4>

<p>
Δωρεές μέσω άλλων μεθόδων μπορεί να επιτευχθούν επικοινωνώντας με τη 
διεύθυνση δωρεών που αναφέρεται στον ιστότοπο του
<a href="https://debian.ch/">debian.ch</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h3 id="debian">Debian</h3>

<p>
Το Debian μπορεί να δεχθεί άμεσες δωρεές σε <a 
href="#debian-equipment">εξοπλισμό</a> αλλά όχι <a 
href="#debian-other">άλλου τύπου</a> δωρεές αυτή τη στιγμή.
</p>

<a name="equipment_donations"></a>
<h4 id="debian-equipment">Εξοπλισμός και υπηρεσίες</h4>

<p>
Το Debian στηρίζεται επίσης στη δωρεά εξοπλισμού και υπηρεσιών από άτομα, 
εταιρείες, πανεπιστήμια κλπ. για να μπορεί να παραμένει συνδεδεμένο με τον 
κόσμο.</p>

<p>Αν η εταιρείας σας έχει οποιαδήποτε μηχανήματα που δεν χρησιμοποιεί ή άλλον 
εξοπλισμό που περισσεύει (σκληρούς δίσκους, ελεγκτές SCSI, κάρτες 
δικτύου, κλπ) παρακαλούμε σκεφτείτε την περίπτωση να τα δωρίσετε στο Debian. 
Παρακαλούμε επικοινωνήστε με την
<a href="mailto:hardware-donations@debian.org">εκπροσώπηση για τις δωρεές 
υλικού</a> για λεπτομέρειες.</p>

<p>Το Debian διατηρεί μια <a 
href="https://wiki.debian.org/Hardware/Wanted">λίστα υλικού που έχει ανάγκη</a> 
για διάφορες υπηρεσίες και ομάδες εντός του Σχεδίου.</p>

<h4 id="debian-time">Χρόνος</h4>

<p>
Υπάρχουν πολλο τρόποι να <a href="$(HOME)/intro/help">βοηθήσετε το 
Debian</a>
χρησιμοποιώντας τον προσωπικό ή εργασιακό σας χρόνο.
</p>

<h4 id="debian-other">Άλλοι τρόποι</h4>

<p>
Το Debian δεν μπορεί να δεχθεί οποιοδήποτε κρυπτονόμισμα αυτή τη στιγμή αλλά 
προσβλέπουμε στο να έχουμε τη δυνατότητα υποστήριξης αυτής της μεθόδου δωρεάς.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>

<h2 id="donors">Δωρητές</h2>

<p>Οι παρακάτω είναι λίστες οργανώεων που έχουν κάνει δωρεές εξοπλισμού ή 
υπηρεσιών στο Debian:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">δωρητές φιλοξενίας 
και υλικού</a></li>
  <li><a href="mirror/sponsors">χορηγοί καθρεφτών της αρχειοθήκης του 
Debian</a></li>
  <li><a href="partners/">συνεργάτες ανάπτυξης και υπηρεσιών</a></li>
</ul>
