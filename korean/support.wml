#use wml::debian::template title="지원"
#use wml::debian::toc
#use wml::debian::translation-check translation="f02dd5c5317327a28418df772a9ca267b58e45ea" maintainer="Sebul"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display />
<toc-add-entry name="doc" href="doc/">머리말</toc-add-entry>

데비안은 자원봉사자의 커뮤니티에 의해 돌아가므로 그 지원도 그러합니다.

커뮤니티 중심의 지원이 여러분의 필요를 만족하지 않는다면, 
<a href="consultants/">consultants</a>에게 연락해도 되고, 또는, 더 좋은 건, 
<a href="doc/">문서와 위키</a>를 읽어 보세요.

<toc-add-entry name="mail_lists" href="MailingLists/">메일링 리스트</toc-add-entry>

<p>데비안은 전세계 분산된 개발을 통해 개발됩니다.
그러므로, 이메일은 다양한 항목을 토론하는 좋은 방법입니다.
데비안 개발자와 사용자의 대화의 많은 부분은 다양한 메일링 리스트를 통해 관리됩니다.
</p>

<p>공개적으로 쓸 수 있는 메일링 리스트가 많습니다.
더 많은 정보는 <a href="MailingLists/">데비안 메일링 리스트</a> 페이지를 보세요.
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>영어로 된 지원에 대해서는, <a href="https://lists.debian.org/debian-user/">데비안 사용자
메일링 리스트</a>에 연락하세요.</p>

<p>다른 언어 지원에 대해서는,<a href="https://lists.debian.org/users.html">사용자 메일링
리스트 색인</a>을 보세요.</p>

<p>물론 많은, 넓은 리눅스 생태계의 어떤 면에 특화된, 데비안에 한정적이지 않은,
메일링 리스트가 있습니다.
좋아하는 검색 엔진으로 여러분의 목적에 적절한 리스트를 찾으세요.</p>

<toc-add-entry name="usenet">유즈넷 뉴스 그룹</toc-add-entry>

<p>많은 <a href="#mail_lists">메일링 리스트</a>를 뉴스 그룹을 통해,
 <kbd>linux.debian.*</kbd> 계층에서 읽을 수 있습니다.
<a href="https://groups.google.com/forum/">구글 그룹</a> 등을 통해 읽을 수도 있습니다.</p>

<toc-add-entry name="forums">Forums</toc-add-entry>

<h3>포럼</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#

<p><a
href="http://forums.debian.net">Debian User Forums</a>는
데비안 관련 주제를 토론하고, 데비안에 대한 문제를 제출하고, 다른 사용자가 답할 수 있는
웹 포털입니다.</p>

<toc-add-entry name="maintainers">패키지 관리자에게 연락하기</toc-add-entry>

<p>패키지 관리자에게 연락하는 방법이 두 가지 있습니다.
버그 때문이라면, 단순히 버그 리포트(아래의 버그 추적 시스템 섹션 보세요)를 제출하세요.
관리자는 버그 보고 사본을 얻을 겁니다.</p>

<p>단순히 관리자와 소통하고 싶으면, 각 패키지마다 메일 별칭을 설정합니다.
&lt;<em>패키지 이름</em>&gt;@packages.debian.org 에 보내진 메일은
 그 패키지에 책임있는 관리자에게 전달될 겁니다.</p>

<toc-add-entry name="bts" href="Bugs/">버그 추적 시스템</toc-add-entry>

<p>데비안 배포본에는 사용자와 개발자가 보고한 버그를 자세히 설명하는 버그 추적 시스템이 있습니다.
각 버그에는 번호가 붙고, 처리된 것으로 표시될 때까지 파일에 남습니다.</p>

<p>버그를 보고하려면, 아래 버그 페이지중 하나를 쓰거나,
데비안 패키지 <q>reportbug</q>를 써서 버그 보고를 자동으로 할 수 있습니다.</p>

<p>버그를 제출하기, 현재 활성화된 버그를 보기, 버그 추적 시스템 일반에 대한 정보는
<a href="Bugs/">버그 추적 시스템 웹 페이지</a>에서 찾을 수 있습니다.</p>

<toc-add-entry name="consultants" href="consultants/">컨설턴트</toc-add-entry>

<p>데비안은 자유 소프트웨어이며 메일링 리스트를 통해 자유롭게 도움을 제공합니다.
어떤 사람들은 시간이 없거나 특별한 필요가 있고 누군가를 고용하여 
데비안 시스템을 유지하고 기능을 추가하길 원할 수 있습니다. 
이런 사람/회사는 <a href="consultants/">컨설턴트 페이지</a>를 보세요.</p>

<toc-add-entry name="irc">IRC를 통한 온라인 실시간 도움</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a>는
전세계 사람들과 실시간 대화를 하기 위한 방법입니다.
데비안을 위한 IRC 채널은 
<a href="https://www.oftc.net/">OFTC</a>에 있습니다.</p>

<p>연결하면, IRC 클라이언트가 필요합니다. 많이 쓰이는 클라이언트는
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> 및
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>이며,
모두 데비안 패키지로 제공됩니다.
OFTC는 로컬 클라이언트 설치 없이 브라우저로 IRC에 연결을 허용하는 
<a href="https://www.oftc.net/WebChat/">WebChat</a>
웹 인터페이스를 제공합니다.

<p>클라이언트를 설치하면, 서버에 연결하도록 얘기해 주어야 합니다.
 대부분의 클라이언트에서, 아래와 같이 칩니다:</p>

<pre>
/server irc.debian.org
</pre>

<p>In some clients (such as irssi) you will need to type this instead:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Once you are connected, join channel <code>#debian</code> by typing</p>

<pre>
/join #debian
</pre>

<p>Note: clients like HexChat often have a different, graphical user interface
for joining servers/channels.</p>

<p>At this point you will find yourself among the friendly crowd of
<code>#debian</code> inhabitants. You're welcome to ask questions about
Debian there. You can find the channel's faq at 
<url "https://wiki.debian.org/DebianIRC" />.</p>


<p>There's a number of other IRC networks where you can chat about Debian,
too. One of the more prominent ones is the
<a href="https://freenode.net/">freenode IRC network</a> at
<kbd>chat.freenode.net</kbd>.</p>

<toc-add-entry name="release" href="releases/stable/">알려진 문제</toc-add-entry>

<p>현재 안정 배포본의 한계화 심각한 문제는 (만약 있다면)
<a href="releases/stable/">릴리스 페이지</a>에 설명합니다.</p>

<p><a href="releases/stable/releasenotes">릴리스
노트</a>와 <a href="releases/stable/errata">정오표</a>를 주의 깊게 보세요.</p>

#########

